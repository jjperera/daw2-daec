//Definicion de las constantes
const cuerpo = document.getElementById('cuerpo');
const total = document.getElementById('total');
const registro = document.getElementById('registro');
const operador = document.getElementById('operador');
const btnInicializar = document.getElementById('inicializar');


//Instanciamos la clase Logger
//MAIN
let logger = new Logger(registro);
let calculadora = new Calculadora();

//------------------------------------------CUANDO SE CARGA----------------------------------------------------
logger.inicializar();
logger.registrarMensaje("Hoja de cálculo inicializada");

//------------------------------------------CUANDO SE RECARGA----------------------------------------------------
window.addEventListener('load', () =>{ //Cuando la pagina se cargue se inicializaran todos los inputs a cero.
    logger.inicializar();
    logger.registrarMensaje("Hoja de cálculo inicializada");
    cuerpo.querySelectorAll('input').forEach( input =>{ 
        input.value=0
    });
//------------------------------------------BOTON RESETEO---------------------------------------------------------
    btnInicializar.addEventListener('click', ()=>{
        location.reload(); //Al darle al boton recargamos la pagina
    });

//------------------------------------------CONTROL DE ERRORES--------------------------------------------------------

    document.body.addEventListener('change', (event) => {
 
});

    //----------------------------------FILAS-------------------------------------------------------------------------
    
    document.querySelectorAll('.operar').forEach(boton => { 
        boton.addEventListener('click', event=>{    //Hacemos un evento con el clik de resultado por fila
           
            let fila = event.currentTarget.parentNode.classList.value;   //Pillamos el value de la clase del padre 

            let hijosFila = document.querySelector('.'+fila).children;  //Añadimos ese String que indica la fila
            calculadora.resultado=1;

            for (let hijo of hijosFila) {
                if(hijo.classList.value.substring(0,1)=='C'){
                    calculadora[operador.value](parseFloat(hijo.value)); //Pasamos los datos de las casillas
                    logger.registrarOperacion(operador.value, parseFloat(hijo.value)); //Lo imprimimos en el logger
                }
                if(hijo.classList.value=="RES"){
                     hijo.value=calculadora.resultado; //Asignamos el resultado de la calculadora
                     logger.registrarResultado(calculadora.resultado); //Imprimimos el resultado 
                }
            }




        });
    });

    //----------------------------------------TOTAL-------------------------------------------------------------------
    total.querySelector('button').addEventListener('click', event =>{   //Hacemos un evento con el clik de resultado total
        logger.registrarMensaje("Calcular el total general");           //Copiamos el codigo anterior
            cuerpo.querySelectorAll('div').forEach(fila =>{

            let hijosFila = document.querySelector('.'+fila.classList).children;
            calculadora.resultado=1;

            for (let hijo of hijosFila) {

                if(hijo.classList.value.substring(0,1)=='C'){
                    calculadora[operador.value](parseFloat(hijo.value));

                }
                if(hijo.classList.value=="RES"){

                     hijo.value=calculadora.resultado;
                     logger.registrarMensaje("El resultado de la fila "+fila.classList+" es "+calculadora.resultado);
                     
                }
            }
        })
        let calculadoraTotal = new Calculadora(); //Inializamos la calculadora otra vez
        cuerpo.querySelectorAll('.RES').forEach(listTotal =>{

            calculadoraTotal[operador.value](parseFloat(listTotal.value)); //Hacemos el calculo de todos los resultados
            logger.registrarOperacion(operador.value, parseFloat(listTotal.value)) //Ponemos en el logger las operaciones realizadas
            
        })

        total.querySelector('input').value=calculadoraTotal.resultado; //Ponemos el resultado en su casilla
        logger.registrarResultado(calculadoraTotal.resultado);  //Registramos el resultado en el logger
        
    });

});

