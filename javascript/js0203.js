//----------------------------------------------------------
// Ordenar palabras
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Constantes
const NUMERO_PALABRAS = 3;

// Array que va a contener las palabras
let palabras = [];

// Lee tres palabras
for(let n = 0;n < NUMERO_PALABRAS;n++) {
    
    let palabra = prompt("Palabra : ");
    palabras.push(palabra);
}

// Las muestra en orden alfabético
palabras = palabras.sort();
for(let p of palabras) {
    console.log(p);
}

// Ahora en orden inverso
for(let p of palabras.reverse()) {
    console.log(p);
}
