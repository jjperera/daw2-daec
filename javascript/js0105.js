//----------------------------------------------------------
// Tipos de motor
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();


let tipoMotor = prompt("Introduce el tipo de motor (0 a 4) : ");
if(tipoMotor == 0) {

    console.log("No hay establecido un valor definido para el tipo de bomba");

} else if(tipoMotor == 1) {

    console.log("La bomba es una bomba de agua");

} else if(tipoMotor == 2) {

    console.log("La bomba es una bomba de gasolina");

} else if(tipoMotor == 3) {

    console.log("La bomba es una bomba de hormigón");

} else if(tipoMotor == 4) {

    console.log("La bomba es una bomba de pasta alimenticia");

} else {
    
    console.log("No existe un valor válido para la bomba");
}