//----------------------------------------------------------
// Calculadora I
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Constantes necesarias
const OPERACIONES_PERMITIDAS = "+-*/CMRq";
const OPERACIONES_UNARIAS = "CMRq";

// Inicializo la pantalla y memoria al valor inicial
let pantalla = 0;
let memoria = 0;

function opSuma(x) {
    pantalla += x;
}

function opResta(x) {
    pantalla -= x;
}

function opMultiplicacion(x) {
    pantalla *= x;
}

function opDivision(x) {
    pantalla /= x;
}

function opMemoria() {
    memoria = pantalla;
}

function opRecupera() {
    pantalla = memoria;
}

function opCero() {
    pantalla = 0;
    memoria = 0;
}



function menuCalculadora() {

    let fin = false;
    while(!fin) {
    
        console.log("pantalla = "+pantalla);
        console.log("memoria = "+memoria);
    
        // Cuando se detecte algún error se va a lanzar una excepción
        try {
            // Lee la entrada que necesita para la operación.
            let operacion = prompt("Operación ( -, +, *, /, C, M, R, q ) :");    
            if(OPERACIONES_PERMITIDAS.indexOf(operacion) < 0) {
                throw "Operación no soportada : "+operacion;
            }
    
            // Si se trata de una operación unaria la ejecuta
            if(OPERACIONES_UNARIAS.indexOf(operacion) >= 0) {
    
                switch(operacion) {
                    case "C": 
                        opCero();
                        break;
                    case "M":
                        opMemoria();
                        break;
                    case "R":
                        opRecupera();
                        break;
                    case "q":
                        fin = true;
                        break;
        
                }            
    
            } else {
    
                // Lee el segundo operando 
                let operando = prompt("Operando : ");
                
                if(Number.isNaN(operando)) {
                    throw "El número introducido no es válido.";
                }
    
                // Conversión explícita a número
                operando = Number(operando);
    
                // Ejecuta la operación
                switch(operacion) {
                    case "+":
                        opSuma(operando);
                        break;
                    case "-":
                        opResta(operando);
                        break;
                    case "*":
                        opMultiplicacion(operando);
                        break;
                    case "/":
                        opDivision(operando);
                        break;
                }
    
            }
    
        } catch(error) {
            console.log("ERROR : "+error);
        }    
    }
}