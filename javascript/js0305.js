//----------------------------------------------------------
// Numero de argumentos variablwe
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();


function sumaNumeros(...numeros) {
    let r = 0;
    for(n of numeros) {
        r += n;
    }

    return r;
}

function multiplicaNumeros(...numeros) {
    let r = 0;
    for(n of numeros) {
        r *= n;
    }

    return r;
}


function minimo(...numeros) {
    let minimo = Infinity;    

    for(n of numeros) {
        if(n < minimo) {
            minimo = n;
        }
    }

    return minimo;
}


function maximo(...numeros) {
    let maximo = -Infinity;    

    for(n of numeros) {
        if(n < maximo) {
            maximo = n;
        }
    }

    return maximo;
}

