// Calcula el factorial de un número de forma iterativa utilizando for
"use strict;"
const prompt = require('prompt-sync')();

// Lee el número. No voy a comprobar errores.
let numero = prompt("Introduce un número : ");

// Calcula el factorial.
let factorial = 1;
for(let n = 2;n <= numero;n++) {
    factorial *= n;
}

// Muestra el resultado
console.log("El factorial es : "+factorial);