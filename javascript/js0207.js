//----------------------------------------------------------
// Football manager
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Guardo el equpo en un Map
let jugadores = new Map();

// Leo los jugadores
let fin = false;
while(!fin) {

    let numero = prompt("Número : ");
    if(numero.length > 0) {

        let nombre = prompt("Nombre : ");

        // Añade el jugador al equipo
        jugadores.set(numero, nombre);    
    } else {
        fin = true;
    }
}

// Permito consultar los jugadores
console.log("Consulta ............... ");
let end = false;
while(!end) {

    let numero = prompt("Número : ");
    if(numero > 0) {

        let nombre = jugadores.get(numero);
        console.log(nombre);

    } else {
        end = true;
    }
}