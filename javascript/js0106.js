//----------------------------------------------------------
// Tipos de motor
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();


let tipoMotor = prompt("Introduce el tipo de motor (0 a 4) : ");

switch(tipoMotor) {
    case 0:
        console.log("No hay establecido un valor definido para el tipo de bomba");
        break;

    case 1:
        console.log("La bomba es una bomba de agua");
        break;

    case 2:
        console.log("La bomba es una bomba de gasolina");
        break;

    case 3:
        console.log("La bomba es una bomba de hormigón");
        break;

    case 4:
        console.log("La bomba es una bomba de pasta alimenticia");
        break;

    default:
        console.log("No existe un valor válido para la bomba");
        break;                            
}
