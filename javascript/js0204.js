//----------------------------------------------------------
// Adivina adivinanza
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Constantes
const MIN_NUMERO = 0;
const MAX_NUMERO = 20;
const NUMEROS_ALEATORIOS = 10;
const NUMEROS_USUARIO = 5;

// Crea las estructuras necesarias
let numeros = new Set();
let intentos = [];
let aciertos = [];

// Genera números aleatorios
let generados = 0;
while(generados < NUMEROS_ALEATORIOS) {
    
    // Genero un número en el rango válido
    let numero = MIN_NUMERO + (Math.random() * (MAX_NUMERO-MIN_NUMERO) );
    
    // Elimina los decimales
    numero = Math.round(numero);

    // Añado el número a la lista de números
    if(!numeros.has(numero)) {
        
        // Si no ha salido antes lo añade e incrementa el contador
        numeros.add(numero);
        generados++;

    }     
}

// Ahora tengo que leer los intentos
for(let n = 0;n < NUMEROS_USUARIO;n++) {
    
    // Lee un número del usuario 
    let numero = prompt("Introduce número : ");
    
    // Lo tiene que convertir a un número
    numero = Number(numero);

    // Añado el número a la lista de números
    intentos.push(numero);
}

// Vamos a comprobar los aciertos
for(let n of intentos) {
    
    // Si el número está en el Set, lo guardo para luego
    if(numeros.has(n)) {
        aciertos.push(n);
    }
}

// Ahora muestra el total y los números
console.log("Aciertos = "+aciertos.length);
for(let n of aciertos) {
    
    console.log(n);
}


