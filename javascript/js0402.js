//----------------------------------------------------------
// Calculadora. new
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Constantes necesarias
const OPERACIONES_PERMITIDAS = "+-*/CMRq";
const OPERACIONES_UNARIAS = "CMRq";

/* 
 * Constructor de la calculadora. Inicializa los atributos y funciones del 
 * objeto 
 */
function Calculadora() {

    /* Pantalla de la calculadora*/
    this.pantalla  = 0;

    /* Memoria de la calculadora */
    this.memoria = 0;

    // Operaciones soportadas por la calculadora 
    // Al ser funciones cortas las defino todas en línea. Si fueran 
    // funciones más largas sería conveniente sacarlas del constructor
    // para facilitar la lectura del programa.
    this["+"] = function (x) { this.pantalla += x; };
    this["-"] = function (x) { this.pantalla -= x; };
    this["*"] = function (x) { this.pantalla *= x; };
    this["/"] = function (x) { this.pantalla /= x; };
    this["M"] = function () { this.memoria = this.pantalla; };
    this["R"] = function () { this.pantalla = this.memoria; };
    this["C"] = function () { 
        this.pantalla = 0;
        this.memoria = 0;
    }
}

function menuCalculadora() {

    let calculadora = new Calculadora();

    let fin = false;
    while(!fin) {
    
        console.log("pantalla = "+calculadora.pantalla);queueMicrotask
        console.log("memoria = "+calculadora.memoria);
    
        // Cuando se detecte algún error se va a lanzar una excepción
        try {
            // Lee la entrada que necesita para la operación.
            let operacion = prompt("Operación ( -, +, *, /, C, M, R, q ) :");    
            if(OPERACIONES_PERMITIDAS.indexOf(operacion) < 0) {
                throw "Operación no soportada : "+operacion;
            }
    
            // Si se trata de una operación unaria la ejecuta
            if(OPERACIONES_UNARIAS.indexOf(operacion) >= 0) {

                if(operacion == "q") {
                    fin = true;
                } else {

                    // Si se trata de una operación, la ejecuto
                    calculadora[operacion]();
                }
    
            } else {
    
                // Lee el segundo operando 
                let operando = prompt("Operando : ");
                
                if(Number.isNaN(operando)) {
                    throw "El número introducido no es válido.";
                }
    
                // Conversión explícita a número
                operando = Number(operando);
    
                // Ejecuta la operación
                calculadora[operacion](operando);
            }
    
        } catch(error) {
            console.log("ERROR : "+error);
        }    
    }
}

menuCalculadora();

