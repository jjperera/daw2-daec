//----------------------------------------------------------
// Calculadora
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Constantes necesarias
const OPERACIONES_PERMITIDAS = "+-*/CMRq";
const OPERACIONES_UNARIAS = "CMRq";

// Inicializo la pantalla y memoria al valor inicial
let pantalla = 0;
let memoria = 0;

let fin = false;
while(!fin) {

    console.log("pantalla = "+pantalla);
    console.log("memoria = "+memoria);

    // Cuando se detecte algún error se va a lanzar una excepción
    try {
        // Lee la entrada que necesita para la operación.
        let operacion = prompt("Operación ( -, +, *, /, C, M, R, q ) :");    
        if(OPERACIONES_PERMITIDAS.indexOf(operacion) < 0) {
            throw "Operación no soportada : "+operacion;
        }

        // Si se trata de una operación unaria la ejecuta
        if(OPERACIONES_UNARIAS.indexOf(operacion) >= 0) {

            switch(operacion) {
                case "C": 
                    pantalla = 0;
                    break;
                case "M":
                    memoria = pantalla;
                    break;
                case "R":
                    pantalla = memoria;
                    break;
                case "q":
                    fin = true;
                    break;
    
            }            

        } else {

            // Lee el segundo operando 
            let operando = prompt("Operando : ");
            
            if(operando == "R") {
                operando = memoria;
            } else if(Number.isNaN(operando)) {
                throw "El número introducido no es válido.";
            }

            // Conversión explícita a número
            operando = Number(operando);

            // Ejecuta la operación
            switch(operacion) {
                case "+":
                    pantalla += operando;
                    break;
                case "-":
                    pantalla -= operando;
                    break;
                case "*":
                    pantalla *= operando;
                    break;
                case "/":
                    pantalla /= operando;
                    break;
            }

        }

    } catch(error) {
        console.log("ERROR : "+error);
    }
}
