//----------------------------------------------------------
// Lee números
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

console.log("Mete los números que quieras. 0 para terminar.");

// Es necesario declarar número antes del bucle, ya que se valida
// fuera de las llaves.
let numero = NaN;

do {

    // Lee un número de la entrada. 
    numero = prompt("Introduce un número : ");

} while(numero != 0)



