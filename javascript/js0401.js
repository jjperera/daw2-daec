//----------------------------------------------------------
// Calculadora. Literal de objeto
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Constantes necesarias
const OPERACIONES_PERMITIDAS = "+-*/CMRq";
const OPERACIONES_UNARIAS = "CMRq";

// Implementación de nuestro objeto calculadora.
let calculadora = {

    /* Pantalla de la calculadora*/
    pantalla : 0,

    /* Memoria de la calculadora */
    memoria : 0,

    // Operaciones soportadas por la calculadora 
    "+" : function (x) { this.pantalla += x; },
    "-" : function (x) { this.pantalla -= x; },
    "*" : function (x) { this.pantalla *= x; },
    "/" : function (x) { this.pantalla /= x; },
    "M" : function () { this.memoria = this.pantalla; },
    "R" : function () { this.pantalla = this.memoria; },
    "C" : function () { 
        this.pantalla = 0;
        this.memoria = 0;
    }
}


function menuCalculadora() {

    let fin = false;
    while(!fin) {
    
        console.log("pantalla = "+calculadora.pantalla);queueMicrotask
        console.log("memoria = "+calculadora.memoria);
    
        // Cuando se detecte algún error se va a lanzar una excepción
        try {
            // Lee la entrada que necesita para la operación.
            let operacion = prompt("Operación ( -, +, *, /, C, M, R, q ) :");    
            if(OPERACIONES_PERMITIDAS.indexOf(operacion) < 0) {
                throw "Operación no soportada : "+operacion;
            }
    
            // Si se trata de una operación unaria la ejecuta
            if(OPERACIONES_UNARIAS.indexOf(operacion) >= 0) {

                if(operacion == "q") {
                    fin = true;
                } else {

                    // Si se trata de una operación, la ejecuto
                    calculadora[operacion]();
                }
    
            } else {
    
                // Lee el segundo operando 
                let operando = prompt("Operando : ");
                
                if(Number.isNaN(operando)) {
                    throw "El número introducido no es válido.";
                }
    
                // Conversión explícita a número
                operando = Number(operando);
    
                // Ejecuta la operación
                calculadora[operacion](operando);
            }
    
        } catch(error) {
            console.log("ERROR : "+error);
        }    
    }
}

menuCalculadora();

