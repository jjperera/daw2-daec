//----------------------------------------------------------
// Cuenta
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

/**
 * Necesito un constructor, de modo que, voy a utilizar una función constructora
 * 
 * @param {*} titular Obligatoria
 * @param {*} cantidad Opcional. Por defecto valdrá 0
 */
function Cuenta(titular, cantidad = 0) {

    // Validación de los parámetros
    if(titular == undefined) {
        throw "El titular es obligatorio";
    }

    if(cantidad < 0) {
        throw "Una cuenta no puede tener un importe negativo";
    }

    // Asigna los valores a las propiedades
    this.titular = titular;
    this.cantidad = cantidad;

    // Define los métodos que son necsarios. Los getters/setters no se necesitan.
    this.toString = function () { return titular+" "+cantidad; }
    this.ingresar = ingresar;
    this.retirar = retirar;
}

/**
 * Ingresa una cantidad en la cuenta. La cantidad no puede ser negativa.
 * 
 * @param {*} cantidad Cantidad a ingresar. Debe ser un valor positivo.
 */

function ingresar(cantidad) {

    if(cantidad < 0) {
        throw "No se puede ingresar una cantidad negativa";
    }

    this.cantidad += cantidad;
}

/**
 * Retira una cantidad de la cuenta. La cuenta solo se puede quedar a 0.
 * 
 * @param {*} cantidad Cantidad a retirar. Será un valor positivo.
 */
function retirar(cantidad) {
    if(cantidad < 0) {
        throw "No se puede retirar una cantidad negativa";       
    }

    // Resta la cantidad
    this.cantidad -= cantidad;

    // Si el resultado es < 0. El resultado es 0
    if(this.cantidad < 0) {
        this.cantidad = 0;
    }
}


// Ejemplo
let cuenta = new Cuenta("Paco");

cuenta.ingresar(10);
cuenta.retirar(1);

console.log(cuenta.cantidad);

cuenta.ingresar(-10);