//----------------------------------------------------------
// Cuenta gallifantes
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

/* La clase cuenta */
class Cuenta {

    /**
     * Crea una instancia de la cuenta
     * 
     * @param {*} titular 
     * @param {*} cantidad 
     */
    constructor(titular, cantidad = 0) {

        // Validación de los parámetros
        if(titular == undefined) {
            throw "El titular es obligatorio";
        }

        if(cantidad < 0) {
            throw "Una cuenta no puede tener un importe negativo";
        }

        // Asigna los valores a las propiedades
        this.titular = titular;
        this.cantidad = cantidad;
    }


    /**
     * Ingresa una cantidad en la cuenta. La cantidad no puede ser negativa.
     * 
     * @param {*} cantidad Cantidad a ingresar. Debe ser un valor positivo.
     */
    ingresar(cantidad) {

        if(cantidad < 0) {
            throw "No se puede ingresar una cantidad negativa";
        }

        this.cantidad += cantidad;
    }


    /**
     * Retira una cantidad de la cuenta. La cuenta solo se puede quedar a 0.
     * 
     * @param {*} cantidad Cantidad a retirar. Será un valor positivo.
     */
    retirar(cantidad) {

        if(cantidad < 0) {
            throw "No se puede retirar una cantidad negativa";       
        }

        // Resta la cantidad
        this.cantidad -= cantidad;

        // Si el resultado es < 0. El resultado es 0
        if(this.cantidad < 0) {
            this.cantidad = 0;
        }
    }

}


class CuentaGallifantes extends Cuenta {

    constructor(titular, cantidad = 0,valor = 1) {
        super(titular, cantidad);

        this.valor = valor;
    }

    ingresar(cantidad) {
        super.ingresar(cantidad * this.valor);
    }

    retirar(cantidad) {
        super.retirar(cantidad * this.valor);
    }
}



// Ejemplo
let cuenta = new CuentaGallifantes("Paco");

cuenta.ingresar(10);
cuenta.retirar(1);

console.log(cuenta.cantidad);

cuenta.ingresar(-10);