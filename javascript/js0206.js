//----------------------------------------------------------
// Filtra letra
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Letras no permitidas
let letrasProhibidas = prompt("Introduce letras prohibidas : ");

// Guarda las palabras y su validez. Al no usar objetos necesito 2 arrays
let palabras = [];
let validez = [];

// Pide palabras y las comprueba
let fin = false;
while(!fin) {

    // Lee una palabra
    let palabra = prompt("Palabra : ");

    // Si la longitud es mayor de 0
    if(palabra.length > 0) {

        palabras.push(palabra);
        
        let prohibidas = 0;

        // Comprueba si la palabra tiene alguno de los caracteres prohibidos    
        for(let ch of palabra) {

            // Si el caracter está, está prohibida
            if(letrasProhibidas.indexOf(ch) >= 0) {

                console.log("Letra prohibida : "+ch);
                prohibidas++;
            }
        }

        // Muestra los errores
        if(prohibidas > 0) {
            validez.push(false);
            console.log("Errores : "+prohibidas);
        } else {
            validez.push(true);
        }

    } else {
        fin = true;
    }
}

// Muestra el resumen final
for(let n = 0;n < palabras.length;n++) {
    console.log(palabras[n]+" "+validez[n]);
}

