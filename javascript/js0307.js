//----------------------------------------------------------
// Contador único
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

contadorUnico.minimo = 0;
contadorUnico.maximo = 100;
contadorUnico.siguiente = contadorUnico.minimo;
function contadorUnico() {

    contadorUnico.siguiente++;
    contadorUnico.siguiente %= contadorUnico.maximo;

    return contadorUnico.siguiente;
}