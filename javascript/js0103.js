// Calcula el factorial de un número de forma iterativa utilizando while
"use strict;"
const prompt = require('prompt-sync')();

// Lee el número. No voy a comprobar errores.
let numero = prompt("Introduce un número : ");

// Calcula el factorial.
let factorial = 1;
let n = 2;
while(n <= numero) {
    
    // Multiplica por el número actual
    factorial *= n;

    // Incrementa el número
    n++;
}

// Muestra el resultado
console.log("El factorial es : "+factorial);