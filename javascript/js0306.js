//----------------------------------------------------------
// Propiedades de una función
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

sumaNumeros.total = 0;
function sumaNumeros(...numeros) {
    let r = 0;
    for(n of numeros) {
        r += n;
    }

    sumaNumeros.total += r;
    return r;
}

multiplicaNumeros.total = 0;
function multiplicaNumeros(...numeros) {
    let r = 0;
    for(n of numeros) {
        r *= n;
    }

    multiplicaNumeros.total *= r;
    return r;
}

minimo.minimo = Infinity;
function minimo(...numeros) {
    let minimo = Infinity;    

    for(n of numeros) {
        if(n < minimo) {
            minimo = n;
        }
    }

    if(minimo < minimo.minimo) {
        minimo.minimo = minimo;
    }
    return minimo;
}

maximo.maximo = -Infinity;
function maximo(...numeros) {
    let maximo = -Infinity;    

    for(n of numeros) {
        if(n < maximo) {
            maximo = n;
        }
    }

    if(maximo > maximo.maximo) {
        maximo.maximo = maximo;
    }
    return maximo;
}

