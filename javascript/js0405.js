//----------------------------------------------------------
// Persona
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

const SEXO_MASCULINO = "M";
const SEXO_FEMENINO = "F";


/**
 * Constructor de un objeto de clase persona.
 * 
 * @param {*} nombre 
 * @param {*} dni 
 * @param {*} sexo 
 * @param {*} edad 
 * @param {*} peso 
 * @param {*} altura 
 */
function Persona(nombre = '',dni = '',sexo = SEXO_MASCULINO,edad = 0,peso = 0,altura = 0) {

    // Valida el dni
    if(!validaDNI(dni)) {
        throw "DNI No válido";
    }

    // Asigna los atributos
    this.nombre = nombre;
    this.dni = dni;
    this.sexo = sexo;
    this.edad = edad;
    this.peso = peso;
    this.altura = altura;

    // Asigna los métodos
    this.calcularIMC = calcularIMC;
    this.esMayorDeEdad = esMayorDeEdad;
    this.validaDNI = validaDNI;
}

/** Calcula el IMC */
function calcularIMC() {
    let imc = this.peso / this.altura**2;
    if(imc < 20) {
        imc = -1;
    } else if(imc <= 25) {
        imc = 0;
    } else {
        imc = 1;
    }
}

/** comprueba si la persona es mayor de edad */
function esMayorDeEdad() {
    return this.edad > 18;
}

function validaDNI(nif) {
    // Si el dni tiene longitud 0 se considera un campo vacío. Retorna true
    if(nif.lenght == 0) {
        return true;
    }
    
    // Letras para el cálculo
    const letras = [ 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E' ];

    // La letra actual
    const letraNif = nif[nif.length-1];

    // Elimina la letra al final del dni
    nif = nif.substring(0, nif.length-1);

    // Calcula el resto
    const resto = nif % 23;

    // Obtiene la letra
    const letraCalculada = letras[resto];

    return letraCalculada == letraNif;
}


console.log(validaDNI("1234S"));
console.log(validaDNI("1234N"));

new Persona("Paco", "1234N");

