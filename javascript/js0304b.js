//----------------------------------------------------------
// Calculadora IIII - Argumentos opcionales
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Constantes necesarias
const OPERACIONES_PERMITIDAS = "+-*/CMRq";
const OPERACIONES_UNARIAS = "CMRq";

// Inicializo la pantalla y memoria al valor inicial
let pantalla = 0;
let memoria = 0;

// Operaciones
let operaciones = new Map();
operaciones.set("+", (x = pantalla) => { pantalla += x; } );
operaciones.set("-", (x = pantalla) => { pantalla -= x; } );
operaciones.set("*", (x = pantalla) => { pantalla *= x; } );
operaciones.set("/", (x = pantalla) => { pantalla /= x; } );
operaciones.set("M", (x = pantalla) => { memoria = pantalla; } );
operaciones.set("R", (x = pantalla) => { pantalla = memoria; } );
operaciones.set("C", (x = pantalla) => { 
    pantalla = 0;
    memoria = 0;
});


function menuCalculadora() {

    let fin = false;
    while(!fin) {
    
        console.log("pantalla = "+pantalla);
        console.log("memoria = "+memoria);
    
        // Cuando se detecte algún error se va a lanzar una excepción
        try {
            // Lee la entrada que necesita para la operación.
            let operacion = prompt("Operación ( -, +, *, /, C, M, R, q ) :");    
            if(OPERACIONES_PERMITIDAS.indexOf(operacion) < 0) {
                throw "Operación no soportada : "+operacion;
            }
    
            // Si se trata de una operación unaria la ejecuta
            if(OPERACIONES_UNARIAS.indexOf(operacion) >= 0) {

                if(operacion == "q") {
                    fin = true;
                } else {

                    // Si se trata de una operación, la ejecuto
                    operaciones.get(operacion)();
                }
    
            } else {
    
                // Lee el segundo operando 
                let operando = prompt("Operando : ");
                
                if(Number.isNaN(operando)) {
                    throw "El número introducido no es válido.";
                }
    
                // Conversión explícita a número
                operando = Number(operando);
    
                // Ejecuta la operación
                operaciones.get(operacion)(operando);    
            }
    
        } catch(error) {
            console.log("ERROR : "+error);
        }    
    }
}

menuCalculadora();
